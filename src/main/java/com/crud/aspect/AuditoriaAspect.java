package com.crud.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuditoriaAspect {

    Logger LOGGER = LoggerFactory.getLogger(AuditoriaAspect.class);

    /** Poincut definition for service methods. */
    public static final String SERVICE_METHOD_POINT_CUT = "execution(* com.crud.service.*.*(..))";

    /**
     * Pointcut for the facade methods.
     */
    @Pointcut(SERVICE_METHOD_POINT_CUT)
    public void serviceMethod() {
        // Empty method definition
    }

    /**
     * Interceptor method
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around("serviceMethod()")
    public Object interceptFacadeMethod(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        Object output = pjp.proceed();
        long elapsedTime = System.currentTimeMillis() - start;

        final Object[] logVars = new Object[] { pjp.getSignature().getDeclaringType().getSimpleName(),
                pjp.getSignature().getName(), pjp.getArgs(), elapsedTime, output == null ? "null" : output.toString() };
        LOGGER.info("[{}.{}({})] Time: [{} ms] Return: [{}]", logVars);
        return output;
    }

}
