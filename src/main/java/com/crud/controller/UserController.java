package com.crud.controller;

import com.crud.model.User;
import com.crud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value={"/index", "/"}, method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("/user/index");
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ModelAndView find(@PathVariable Long id){
        userService.findUserBydId(id);
        return new ModelAndView("/user/index");
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
    public ModelAndView remove(@PathVariable Long id){
        userService.removeUser(id);
        return new ModelAndView("/user/index");
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable Long id){
        User user = new User();
        user.setEmail("cosita@gmail.com");
        user.setName("Cosita");
        user.setIdUser(id);
        userService.updateUser(user);
        return new ModelAndView("/user/index");
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create(){

        User user = new User();
        user.setEmail("cosita@gmail.com");
        user.setName("Cosita");
        user.setIdUser(2l);
        userService.saveUser(user);
        return new ModelAndView("/user/index");
    }

}
