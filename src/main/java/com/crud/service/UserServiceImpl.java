package com.crud.service;

import com.crud.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    public static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public void saveUser(User user) {
        LOGGER.info("Guardando usuario ");
    }

    @Override
    public void updateUser(User user) {
        LOGGER.info("Actualizando usuario ");
    }

    @Override
    public void removeUser(Long id) {
        LOGGER.info("Eliminando usuario ");
    }

    @Override
    public void findUserBydId(Long id) {
        LOGGER.info("Buscando usuario ");
    }
}
