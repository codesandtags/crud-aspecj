package com.crud.service;

import com.crud.model.User;

/**
 * Created by gergom on 13/05/14.
 */
public interface UserService {

    void saveUser(User user);
    void updateUser(User user);
    void removeUser(Long id);
    void findUserBydId(Long id);

}
